//
//  AlertHandler.swift
//  Kollegue
//
//  Created by Mujib Saiyyed on 10/8/20.
//  Copyright © 2020 Mujib Saiyyed. All rights reserved.
//

import UIKit

class AlertHandler {
    
    class func showAlert(owner: UIViewController, title: String, message: String, alertStyle: UIAlertController.Style) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: alertStyle)
        let actionOne = UIAlertAction(title: "Let me retry", style: .cancel)
        alert.addAction(actionOne)
        owner.present(alert, animated: true, completion: nil)
    }
}
