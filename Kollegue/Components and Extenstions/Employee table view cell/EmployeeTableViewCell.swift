//
//  EmployeeTableViewCell.swift
//  Kollegue
//
//  Created by Mujib Saiyyed on 9/28/20.
//  Copyright © 2020 Mujib Saiyyed. All rights reserved.
//

import UIKit

class EmployeeTableViewCell: UITableViewCell {

    @IBOutlet weak var avatarBackground: UIView!
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var technology: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func draw(_ rect: CGRect) {
        self.avatar.layer.cornerRadius = self.avatar.frame.width/2
        self.avatarBackground.layer.cornerRadius = self.avatarBackground.frame.width/2
        self.dropShadowFor()
    }
    func dropShadowFor() {
        self.avatarBackground.layer.masksToBounds = false
        self.avatarBackground.layer.shadowColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1).cgColor
        self.avatarBackground.layer.shadowOpacity = 0.5
        self.avatarBackground.layer.shadowOffset = CGSize(width: 1, height: 1)
        self.avatarBackground.layer.shadowRadius = 1
    }
}
