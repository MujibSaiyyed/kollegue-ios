//
//  UIButton + Exntension.swift
//  Kollegue
//
//  Created by Mujib Saiyyed on 10/8/20.
//  Copyright © 2020 Mujib Saiyyed. All rights reserved.
//

import UIKit

@IBDesignable
extension UIButton {
    
    @IBInspectable
    var dropShadow: Bool {
        get {
            return self.layer.shadowRadius > 0
        }
        set {
            self.layer.masksToBounds = false
            self.layer.shadowColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1).cgColor
            self.layer.shadowOpacity = 0.5
            self.layer.shadowOffset = CGSize(width: 1, height: 1)
            self.layer.shadowRadius = 1

        }
    }
}
