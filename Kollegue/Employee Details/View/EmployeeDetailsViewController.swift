//
//  EmployeeDetailsViewController.swift
//  Kollegue
//
//  Created by Mujib Saiyyed on 9/28/20.
//  Copyright © 2020 Mujib Saiyyed. All rights reserved.
//

import UIKit

enum DetailsFor {
    case others
    case own
}

class EmployeeDetailsViewController: UIViewController {
    @IBOutlet private weak var avatar: UIImageView!
    @IBOutlet private weak var name: UILabel!
    @IBOutlet private weak var technology: UILabel!
    
    private lazy var blurView = UIView()
    private lazy var expandedAvatarView = UIImageView()
    
    func instantiateWith(model: EmployeeDetails) -> EmployeeDetailsViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let controller = storyboard.instantiateViewController(withIdentifier: "EmployeeDetailsViewController") as? EmployeeDetailsViewController else { return EmployeeDetailsViewController() }
//        if let image = model.avatar {
//            controller.avatar.image = UIImage(named: image)
//        }
//        controller.name.text = model.name
//        controller.technology.text = model.technology
        return controller
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Details"
        self.setupUI()
        
        
    }
    
    func setupUI() {
        self.avatar.layer.cornerRadius = self.avatar.frame.width/2
        self.dropShadowFor()
    }
    
    func dropShadowFor() {
//        self.avatarBackground.layer.masksToBounds = false
//        self.avatarBackground.layer.shadowColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1).cgColor
//        self.avatarBackground.layer.shadowOpacity = 0.5
//        self.avatarBackground.layer.shadowOffset = CGSize(width: 1, height: 1)
//        self.avatarBackground.layer.shadowRadius = 1
    }
    
    @IBAction func didSelectAvatar(_ sender: Any) {
        blurView = UIView(frame: self.view.frame)
        blurView.backgroundColor = .black
        blurView.alpha = 0.7
        blurView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(removeImage)))
        self.view.addSubview(blurView)
        expandedAvatarView = UIImageView(frame: CGRect(x: self.view.center.x - 200, y: self.view.center.y - 200, width: 400, height: 400))
        expandedAvatarView.contentMode = .scaleAspectFit
        expandedAvatarView.image = UIImage(named: "ms1")
        
        self.view .addSubview(expandedAvatarView)
        
    }
    
    @objc private func removeImage() {
        self.blurView.removeFromSuperview()
        self.expandedAvatarView.removeFromSuperview()
    }
    
}
