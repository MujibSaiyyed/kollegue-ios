//
//  HomeModel.swift
//  Kollegue
//
//  Created by Mujib Saiyyed on 9/27/20.
//  Copyright © 2020 Mujib Saiyyed. All rights reserved.
//

struct HomeModel: Equatable {
    let details: [EmployeeDetails]
}

struct EmployeeDetails: Equatable {
    let name: String?
    let technology: String?
    let employeeId: String?
    let avatar: String?
}
