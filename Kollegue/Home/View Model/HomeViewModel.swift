//
//  HomeViewModel.swift
//  Kollegue
//
//  Created by Mujib Saiyyed on 9/27/20.
//  Copyright © 2020 Mujib Saiyyed. All rights reserved.
//

class HomeViewModel {
    
//    var employeeDetailsArray: HomeModel?
    
    
    func getEmployeeDetails(searchString: String) -> HomeModel {
        
        let emp1 = EmployeeDetails(name: "Mujib Saiyyed", technology: "iOS Dev", employeeId: "ABC1", avatar: "ms1")
        let emp2 = EmployeeDetails(name: "Rugved", technology: "Android", employeeId: "ABC2", avatar: "ms2")
        let emp3 = EmployeeDetails(name: "Mujib Saiyyed", technology: "iOS Dev", employeeId: "ABC1", avatar: "ms1")
        let emp4 = EmployeeDetails(name: "Rugved", technology: "Android", employeeId: "ABC2", avatar: "ms2")
        let emp5 = EmployeeDetails(name: "Mujib Saiyyed", technology: "iOS Dev", employeeId: "ABC1", avatar: "ms1")
        let emp6 = EmployeeDetails(name: "Rugved", technology: "Android", employeeId: "ABC2", avatar: "ms2")
        return HomeModel(details: [ emp1, emp2, emp3, emp4, emp5, emp6])
    }
}
