//
//  HomeViewController.swift
//  Kollegue
//
//  Created by Mujib Saiyyed on 9/24/20.
//  Copyright © 2020 Mujib Saiyyed. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak var mainTableView: UITableView!
    
    var employeeDetailsArray: HomeModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.registerCells()
    }

    private func registerCells() {
        let nib = UINib(nibName: "EmployeeTableViewCell", bundle: nil)
        self.mainTableView.register(nib, forCellReuseIdentifier: "employeeCell")
        
    }
    
    @IBAction func didSelectProfileButton(_ sender: Any) {
        
        let userSession = UserSession.shared
        let session = UserSession.session
        let controller = EmployeeDetailsViewController().instantiateWith(model: EmployeeDetails(name: userSession.user?.emailID, technology: userSession.user?.userTechnology, employeeId: userSession.user?.userID, avatar: userSession.user?.userAvatar))
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
}

extension HomeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (employeeDetailsArray?.details.count) ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "employeeCell") as? EmployeeTableViewCell else { return UITableViewCell() }
        cell.name.text = employeeDetailsArray?.details[indexPath.row].name
        cell.technology.text = employeeDetailsArray?.details[indexPath.row].technology
        if let image = employeeDetailsArray?.details[indexPath.row].avatar {
            cell.avatar.image = UIImage(named: image)
        }
        if indexPath.row % 2 == 0 {
            cell.backgroundColor = #colorLiteral(red: 0.9289208109, green: 0.9289208109, blue: 0.9289208109, alpha: 1)
        }
        return cell
    }
    
    
}

extension HomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 96
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let user = employeeDetailsArray?.details[indexPath.row] else { return }
        let controller =  EmployeeDetailsViewController().instantiateWith(model: EmployeeDetails(name: user.name, technology: user.technology, employeeId: user.employeeId, avatar: user.avatar))
        self.navigationController?.pushViewController(controller, animated: true)
    }
}

extension HomeViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let searchText = searchBar.text else { return }
        employeeDetailsArray = HomeViewModel().getEmployeeDetails(searchString: searchText)
        self.mainTableView.reloadData()
        searchBar.resignFirstResponder()
        self.mainTableView.isHidden = false
        searchBar.text = ""
    }
}

