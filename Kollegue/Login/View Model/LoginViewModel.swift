//
//  LoginViewModel.swift
//  Kollegue
//
//  Created by Mujib Saiyyed on 9/27/20.
//  Copyright © 2020 Mujib Saiyyed. All rights reserved.
//

import UIKit
import Firebase

class LoginViewModel {
    
    private let expectedEmail = "mujib@gmail.com"
    private let expectedPssword = "Amla@987"
    
    func authorizeUserWith(email: String, password:String) -> Bool {
//        Firebase.Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
//            print(result?.additionalUserInfo.debugDescription as Any)
//        }
        
        if email == self.expectedEmail,
        password == self.expectedPssword {
            let userDefaults = UserDefaults.standard
            
            let userSession = UserSession.shared
            userSession.user = User(emailID: email, userID: "ABC123", usertype: UserType.employee, userTechnology: "iOS Engineer", userAvatar: "ms1")
            
            userSession.isLoggedin = true
            
            //        userDefaults.set(userSession.user, forKey: "UserDetails")
            userDefaults.set(userSession.isLoggedin, forKey: "isUserLoggedIn")
            
            return true
        }
        
        return false
    }
    
    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
}
