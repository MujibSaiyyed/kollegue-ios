//
//  LoginViewController.swift
//  Kollegue
//
//  Created by Mujib Saiyyed on 9/27/20.
//  Copyright © 2020 Mujib Saiyyed. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet private weak var loginButton: UIButton!
    @IBOutlet private weak var usernameTextField: UITextField!
    @IBOutlet private weak var passwordTextField: UITextField!
    @IBOutlet private weak var logoLabel: UILabel!
    
    private var isAlreadyAnimated: Bool = false
    private var viewModel = LoginViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        SingletonDemo.shared.someVar
    }
    
    override func viewDidLayoutSubviews() {
        super.viewWillLayoutSubviews()
        if !self.isAlreadyAnimated {
            self.animate()
            self.isAlreadyAnimated = true
        }
    }
    
    private func setupUI() {
        self.loginButton.layer.cornerRadius = 5
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(resignKeyboard)))
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
    }
    
    @objc private func resignKeyboard() {
        self.usernameTextField.resignFirstResponder()
        self.passwordTextField.resignFirstResponder()
    }
    
    private func animate() {
        self.loginButton.alpha = 0
        self.usernameTextField.alpha = 0
        self.usernameTextField.isEnabled = false
        self.passwordTextField.alpha = 0
        self.passwordTextField.isEnabled = false
        self.logoLabel.isHidden = true
        
        let newLogo = UILabel(frame: self.logoLabel.frame)
        newLogo.text = "Kollegue"
        newLogo.font = UIFont(name: "Marker Felt", size: 33)
        newLogo.textColor = .darkGray
        newLogo.center = self.view.center
        
        self.view.addSubview(newLogo)
        
        UIView.animate(withDuration: 0.5, animations: {
            newLogo.center = self.logoLabel.center
            
        }) { (isCompleted) in
            self.logoLabel.isHidden = false
            newLogo.removeFromSuperview()
            UIView.animate(withDuration: 0.1, animations: {
                
                self.usernameTextField.alpha = 1
                
            }) { (isCompleted) in
                
                UIView.animate(withDuration: 0.1, animations: {
                
                    self.passwordTextField.alpha = 1
                    self.usernameTextField.isEnabled = true
                
                }) { (isComplted) in
                
                    UIView.animate(withDuration: 0.1) {
                    
                        self.loginButton.alpha = 0.4
                        self.passwordTextField.isEnabled = true
                    
                    }
                }
            }
        }
    }
    
    private func setLoginButtonInteration(isEnabled: Bool) {
        
        self.loginButton.isEnabled = isEnabled
        if isEnabled {
            self.loginButton.alpha = 1
        } else {
            self.loginButton.alpha = 0.4
        }
    }
    
    @IBAction func valueChangesForTextField() {
        if self.usernameTextField.text?.count ?? 0 > 4, self.passwordTextField.text?.count ?? 0 > 4, viewModel.isValidEmail(self.usernameTextField.text ?? "") {
            self.setLoginButtonInteration(isEnabled: true)
        } else {
            self.setLoginButtonInteration(isEnabled: false)
        }
    }
    
    @IBAction func didSlelectLogin(_ sender: Any) {
        
        guard let email = usernameTextField.text,
            let password = passwordTextField.text else {
            //Show alert and return
                return
        }
        
        let isUserValid = self.viewModel.authorizeUserWith(email: email, password: password)
        
        if isUserValid {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "HomeViewController")
            self.navigationController?.pushViewController(controller, animated: true)
        } else {
            AlertHandler.showAlert(owner: self, title: "Oops!", message: "Something doesn't seem alright. Please check you emailID or password.\nIn case of any issue, contact your HR.", alertStyle: .actionSheet)
        }
    }
}

extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
}
