//
//  UserSession.swift
//  Kollegue
//
//  Created by Mujib Saiyyed on 9/29/20.
//  Copyright © 2020 Mujib Saiyyed. All rights reserved.
//

class UserSession {
    private init() {
    }
    static let shared = UserSession()
    static var session = UserSession()
    var user: User?
    var isLoggedin: Bool?
    
}

enum UserType {
    case employee
    case hr
}

struct User {
    let emailID: String
    let userID: String
    let usertype: UserType
    let userTechnology: String
    let userAvatar: String
}
