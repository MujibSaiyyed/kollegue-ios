//
//  HomeViewModelTests.swift
//  KollegueTests
//
//  Created by Mujib Saiyyed on 10/8/20.
//  Copyright © 2020 Mujib Saiyyed. All rights reserved.
//

import XCTest
@testable import Kollegue

class HomeViewModelTests: XCTestCase {

    
    var empArray: HomeModel!
    
    override func setUp() {
        let emp1 = EmployeeDetails(name: "Mujib Saiyyed", technology: "iOS Dev", employeeId: "ABC1", avatar: "ms1")
        let emp2 = EmployeeDetails(name: "Rugved", technology: "Android", employeeId: "ABC2", avatar: "ms2")
        let emp3 = EmployeeDetails(name: "Mujib Saiyyed", technology: "iOS Dev", employeeId: "ABC1", avatar: "ms1")
        let emp4 = EmployeeDetails(name: "Rugved", technology: "Android", employeeId: "ABC2", avatar: "ms2")
        let emp5 = EmployeeDetails(name: "Mujib Saiyyed", technology: "iOS Dev", employeeId: "ABC1", avatar: "ms1")
        let emp6 = EmployeeDetails(name: "Rugved", technology: "Android", employeeId: "ABC2", avatar: "ms2")
        self.empArray = HomeModel(details: [ emp1, emp2, emp3, emp4, emp5, emp6])
    }
    
    func testGetEmployeeDetails() {
        let returnValue = HomeViewModel().getEmployeeDetails(searchString: "")
        XCTAssertEqual(returnValue, self.empArray)
    }
    
    override func tearDown() {
        self.empArray = nil
    }
}
