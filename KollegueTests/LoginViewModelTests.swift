//
//  LoginViewModelTests.swift
//  LoginViewModelTests
//
//  Created by Mujib Saiyyed on 9/24/20.
//  Copyright © 2020 Mujib Saiyyed. All rights reserved.
//

import XCTest
@testable import Kollegue

class LoginViewModelTests: XCTestCase {

    var email: String!
    var password: String!
    var loginViewModel: LoginViewModel!
    
    override func setUp() {
        self.email = "mujib@gmail.com"
        self.password = "Amla@987"
        self.loginViewModel = LoginViewModel()
    }

    override func tearDown() {
        self.email = nil
        self.password = nil
        self.loginViewModel = nil
    }

    func testAuthorizeUser_forTrue() {
        let returnValue = self.loginViewModel.authorizeUserWith(email: self.email, password: self.password)
        XCTAssertTrue(returnValue)
    }
    
    func testAuthorizeUser_forFalse() {
        self.email = "wrong@email.com"
        self.password = "wrongPassword"
        let returnValue = self.loginViewModel.authorizeUserWith(email: self.email, password: self.password)
        XCTAssertFalse(returnValue)
    }
    
    func testIsValidEmail_forTrue() {
        self.email = "Mujib@saiyyed.com"
        let isEmailValid = self.loginViewModel.isValidEmail(self.email)
        XCTAssertTrue(isEmailValid)
    }
    
    func testIsValidEmail_forFalse() {
        self.email = "wrong@email"
        let isEmailValid = self.loginViewModel.isValidEmail(self.email)
        XCTAssertFalse(isEmailValid)
    }
    

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
