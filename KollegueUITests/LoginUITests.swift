//
//  LoginUITests.swift
//  LoginUITests
//
//  Created by Mujib Saiyyed on 9/24/20.
//  Copyright © 2020 Mujib Saiyyed. All rights reserved.
//

import XCTest

class LoginUITests: XCTestCase {

    var expectedEmail: String!
    var expectedPassword: String!
    var wrongEmail: String!
    var wrongPassword: String!
    
    override func setUp() {
        self.expectedEmail = "mujib@gmail.com"
        self.expectedPassword = "Amla@987"
        self.wrongEmail = "asa@asd.cad"
        self.wrongPassword = "asdds"
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testLoginFailure_invalidCredentials() {
        
        let app = XCUIApplication()

        let loginButton = app.buttons["Login"]
        XCTAssertTrue(loginButton.exists, "Login Button doesn't exist")
        XCTAssertFalse(loginButton.isEnabled, "Login button enabled, when it shouldn't")

        let emailTextField = app.textFields["Email Id"]
        XCTAssertTrue(emailTextField.exists, "email textfield doesn't exist")
        emailTextField.tap()
        emailTextField.typeText(self.wrongEmail)

        let passwordSecureTextField = app.secureTextFields["Password"]
        XCTAssertTrue(passwordSecureTextField.exists, "password textfield doesn't exist")
        passwordSecureTextField.tap()
        passwordSecureTextField.typeText(wrongPassword)

        XCTAssertTrue(loginButton.isEnabled, "Login Button still disabled")
        loginButton.tap()

        let alert = app.sheets["Oops!"]
        XCTAssertTrue(alert.exists, "Alert doesn't exist")
        let retryButton = alert.scrollViews.otherElements.buttons["Let me retry"]
        XCTAssertTrue(retryButton.exists, "Alert button doesn't exist")
        retryButton.tap()

        XCTAssertTrue(loginButton.exists, "Login button doesn't exist")
    }
    
    func testLoginFailure_ValidCredentials() {
        
        let app = XCUIApplication()
        
        let loginButton = app.buttons["Login"]
        XCTAssertTrue(loginButton.exists, "Login Button doesn't exist")
        XCTAssertFalse(loginButton.isEnabled, "Login button enabled, when it shouldn't")
        
        let emailTextField = app.textFields["Email Id"]
        XCTAssertTrue(emailTextField.exists, "email textfield doesn't exist")
        emailTextField.tap()
        emailTextField.typeText(self.expectedEmail)
        
        let passwordSecureTextField = app.secureTextFields["Password"]
        XCTAssertTrue(passwordSecureTextField.exists, "password textfield doesn't exist")
        passwordSecureTextField.tap()
        passwordSecureTextField.typeText(expectedPassword)
        
        XCTAssertTrue(loginButton.isEnabled, "Login Button still disabled")
        loginButton.tap()
        
        let searchBar = app.searchFields["Search a Kollegue"]
        XCTAssertTrue(searchBar.exists)
        
    }
    
    func testLaunchPerformance() {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, *) {
            // This measures how long it takes to launch your application.
            measure(metrics: [XCTOSSignpostMetric.applicationLaunch]) {
                XCUIApplication().launch()
            }
        }
    }
}
